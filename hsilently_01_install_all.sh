#!/bin/bash

sudo bash ./_ubuntu_DO_ALL/01_02_update_ubuntu.sh
sudo bash ./_ubuntu_DO_ALL/02_01_install_openjdk.sh
sudo bash ./_ubuntu_DO_ALL/02_02_install_maven_AND_IDE.sh
sudo bash ./_ubuntu_DO_ALL/02_03_install_tomcat.sh
sudo bash ./_ubuntu_DO_ALL/02_04_add_tomcat_user.sh
sudo bash ./_ubuntu_DO_ALL/03_01_vi_tomcat_service.sh
sudo bash ./_ubuntu_DO_ALL/03_02_systemctl_start_tomcat.sh
sudo bash ./_ubuntu_DO_ALL/04_01_install_docker.sh
sudo bash ./_ubuntu_DO_ALL/04_02_using_docker_cmd_AS_ROOT.sh
sudo bash ./_ubuntu_DO_ALL/05_01_install_ansible_AS_ROOT.sh
sudo bash ./_ubuntu_DO_ALL/06_01_install_terraform_AS_ROOT.sh
sudo bash ./_ubuntu_DO_ALL/07_01_install_packer_AS_ROOT.sh
sudo bash ./_ubuntu_DO_ALL/08_01_install_kubernetes.sh

# sudo bash ./_ubuntu_DO_ALL/08_02_sudo_kubeadm_join.sh
echo "# sudo bash ./_ubuntu_DO_ALL/08_02_sudo_kubeadm_join.sh"

# sudo bash jsilently_MVN_build_all_deploy_and_run_EN.sh
