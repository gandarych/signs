#!/bin/bash

sudo bash ./_ubuntu_DO_ALL/01_02_update_ubuntu.sh

sudo bash ./_ubuntu_DO_ALL/02_01_install_openjdk.sh

sudo bash ./_ubuntu_DO_ALL/02_02_install_maven.sh

sudo bash ./_ubuntu_DO_ALL/02_03_install_tomcat.sh

sudo bash ./_ubuntu_DO_ALL/02_04_add_tomcat_user.sh

## Modify the content of tomcat.service
cat ./_ubuntu_DO_ALL/03_01_vi_tomcat_service.txt

## sudo bash ./_ubuntu_DO_ALL/03_02_systemctl_start_tomcat.sh
