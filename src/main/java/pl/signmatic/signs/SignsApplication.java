package pl.signmatic.signs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SignsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SignsApplication.class, args);
	}

	@GetMapping("/hullo")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hullo %s! Signs app is alive!", name);
	}

}
